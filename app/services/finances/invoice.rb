# frozen_string_literal: true

module Finances
  class Invoice
    def initialize(user:, invoice:)
      @user = user
      @invoice = invoice
    end

    def add_income_record
      income = create_income

      month.incomes << income
    end

    attr_reader :user, :invoice

    private

    def create_income
      Income.create(
        date_of: invoice.paid_on,
        summary: "##{invoice.id}: #{invoice.title}",
        category: invoice.category,
        balance: invoice.balance,
        vat: invoice.vat
      )
    end

    def year
      financial_year = Finances::FinancialYear.new(account: account, date: invoice.paid_on)
      financial_year.find_or_create
    end

    def month
      year.months.where(month: invoice_month).first
    end

    def invoice_month
      invoice.paid_on.strftime("%B '%y")
    end

    def account
      Account.find(user.selected_account)
    end
  end
end
