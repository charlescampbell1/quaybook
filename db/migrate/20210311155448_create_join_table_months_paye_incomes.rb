class CreateJoinTableMonthsPayeIncomes < ActiveRecord::Migration[6.0]
  def change
    create_join_table :months, :paye_incomes do |t|
      t.index [:month_id, :paye_income_id]
      t.index [:paye_income_id, :month_id]
    end
  end
end
