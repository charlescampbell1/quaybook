# frozen_string_literal: true

class NotificationsController < ApplicationController
  before_action :authenticate_user!, :active_user?, :active_account?
  before_action :set_notification, only: %i[read]

  def index
    @pagy, @notifications = pagy(current_user.notifications.order('created_at DESC'), items: 15)

    render 'components/pages/notification/index'
  end

  def read
    @notification.read = true
    @notification.save

    redirect_to notification_path, notice: 'Marked as read'
  end

  def read_all
    current_user.notifications.each do |notification|
      notification.read = true
      notification.save
    end

    redirect_to notification_path, notice: 'All notifications marked as read'
  end

  private

  def set_notification
    @notification = Notification.find(params[:id])
  end
end
