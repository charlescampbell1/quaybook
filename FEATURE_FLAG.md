# Feature Flags

This document contains detail on how to enable, create and disable feature flags.

---

## Production Usage

This is a detailed list of the current features flags in production. This can be controlled on an account by account basis. They must be removed with caution.

### Current Implementations

| Feature Flag | Code Identifier |
| ----------- | ----------- |
| Invoices | invoice |
| PAYE Income | paye_income |

## Development

### New Feature Flag

In order to create a new feature flag there are a few steps. By default feature flags are disabled for each account.

1. Specify a code identifier and a description for the feature in this document.
2. Add the feature flag to production and staging.
  a. Go to DOMAIN_URL/features.
  b. Click `Add Feature`.
  c. Carefully fill out the feature and code identifier information.
3. Wrap the code using the `feature_enabled?` method.
4. Feature flag is ready for use.

### Removing Feature Flag

In order to remove the feature flag it must be removed from the code base.

1. Remove all references to the features code identifier from the code base.
2. Remove the feature flag from both production and staging.
  a. Go to DOMAIN_URL/features
  b. Click delete on the feature that needs to be removed.
3. Feature flag is ready for use.
