# frozen_string_literal: true

require 'trello'
class SupportController < ApplicationController
  before_action :authenticate_user!, :active_user?, :active_account?

  include SupportHelper

  def index
    render 'components/pages/support/index'
  end

  # :nocov:
  def raise_ticket
    ticket = raise_trello_ticket(label: params[:label], title: params[:title], description: params[:description])

    return redirect_to support_path, notice: 'Ticket created. You can view outstanding tickets below.' if ticket

    redirect_to support_path, notice: 'Something has gone wrong. Please contact us via our phoneline.'
  end
  # :nocov:
end
