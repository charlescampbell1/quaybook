# frozen_string_literal: true

Rails.application.routes.draw do
  # Users Configuration
  get 'holding', to: 'dashboard#holding', as: :holding
  get 'people', to: 'users#index', as: :people

  devise_for :users
  resources :users
  scope :user do
    get 'delete/:id', to: 'users#destroy', as: :destroy_user
  end

  # Profile Configuration
  get 'profile', to: 'users#profile', as: :profile
  patch 'profile', to: 'users#update_profile', as: :update_profile
  get 'profile_picture', to: 'users#profile_picture', as: :edit_profile_picture
  delete 'profile_picture', to: 'users#remove_profile_picture', as: :remove_profile_picture

  # Dashboard Configuration
  root 'dashboard#index'
  get 'dashboard', to: 'dashboard#index', as: :dashboard

  # Account Configuration
  get 'accounts', to: 'accounts#index', as: :account
  patch 'account/:id', to: 'accounts#update', as: :update_account
  resources :accounts

  scope :account do
    get 'setup', to: 'accounts#setup', as: :account_setup
    get 'add_users/:id', to: 'accounts#add_users', as: :add_users
    get 'remove/:user_id/from/:account_id', to: 'accounts#remove_user', as: :remove_user
    get 'add/:user_id/to/:account_id', to: 'accounts#add_user', as: :add_user
    get 'delete/:id', to: 'accounts#destroy', as: :destroy_account
    get 'switch', to: 'accounts#switch', as: :switch
    get 'switch/:id', to: 'accounts#switch_account', as: :switch_account
    get 'show/:id', to: 'accounts#show', as: :show_account
  end

  # Bookkeeping Configuration
  get 'bookkeeping', to: 'bookkeeping#index', as: :bookkeeping

  scope :bookkeeping do
    get 'new_year', to: 'bookkeeping#new_year', as: :new_year
    get 'create_year', to: 'bookkeeping#index', as: :years
    post 'create_year', to: 'bookkeeping#create_year', as: :create_year

    get 'year/:id', to: 'bookkeeping#show_year', as: :show_year
    get 'month/:id', to: 'bookkeeping#show_month', as: :show_month

    # Income Configuration
    scope :income do
      get 'new/:month_id', to: 'income#new', as: :new_income
      post 'create_income', to: 'income#create', as: :create_income
      get 'edit/:id', to: 'income#edit', as: :edit_income
      patch 'update_income/:id', to: 'income#update', as: :update_income
      delete ':id', to: 'income#destroy', as: :destroy_income
    end

  # Expenses Configuration
    scope :expense do
      get 'new/:month_id', to: 'expenses#new', as: :new_expense
      post 'create_expenses', to: 'expenses#create', as: :create_expense
      get 'edit/:id', to: 'expenses#edit', as: :edit_expense
      patch 'update_expenses/:id', to: 'expenses#update', as: :update_expense
      delete ':id', to: 'expenses#destroy', as: :destroy_expense
    end
  end

  # Support Configuration
  get 'support', to: 'support#index', as: :support
  post 'support', to: 'support#raise_ticket', as: :raise_ticket

  # Invoice Configuration
  get 'invoice', to: 'invoices#index', as: :invoice

  scope :invoice do
    get 'show/:id', to: 'invoices#show', as: :show_invoice
    get 'request/:id', to: 'invoices#request_approval', as: :request_approval
    get 'approve/:id', to: 'invoices#approve', as: :approve_invoice
    get 'reject/:id', to: 'invoices#reject', as: :reject_invoice
    get 'pay/:id', to: 'invoices#pay', as: :pay_invoice_confirm
    patch 'paid/:id', to: 'invoices#paid', as: :pay_invoice
    get 'destroy/:id', to: 'invoices#destroy', as: :destroy_invoice
    patch ':id', to: 'invoices#update', as: :update_invoice

    scope :item do
      get 'new/:invoice_id', to: 'items#new', as: :new_item
      get 'edit/:id', to: 'items#edit', as: :edit_item
      post 'new', to: 'items#create', as: :create_item
      patch 'edit/:id', to: 'items#update', as: :update_item
      delete ':id', to: 'items#destroy', as: :destroy_item
    end
  end

  resources :invoices

  # PAYE Income Configuration
  get 'paye_income', to: 'paye_incomes#index', as: :paye_income

  scope :paye_income do
    delete 'delete/:id', to: 'paye_incomes#destroy', as: :destroy_paye_income
    patch 'update/:id', to: 'paye_incomes#update', as: :update_paye_income
  end

  resources :paye_incomes

  # Notification Actions
  get 'notifications', to: 'notifications#index', as: :notification

  scope :notification do
    get 'read/:id', to: 'notifications#read', as: :read_notification
    get 'read_all', to: 'notifications#read_all', as: :read_all_notifications
  end

  # Features Configuration
  get 'features', to: 'features#index', as: :features

  scope :feature do
    get 'show/:id', to: 'features#show', as: :show_feature
    delete 'delete/:id', to: 'features#destroy', as: :destroy_feature
    get 'enable/:id/:account_id', to: 'features#enable', as: :enable_feature
    get 'disable/:id/:account_id', to: 'features#disable', as: :disable_feature
  end

  resources :features
end
