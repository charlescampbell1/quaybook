class CreateJoinTableMonthsYears < ActiveRecord::Migration[6.0]
  def change
    create_join_table :months, :years do |t|
      t.index [:month_id, :year_id]
      t.index [:year_id, :month_id]
    end
  end
end
