# frozen_string_literal: true

module TaxHelper
  BASIC_RATE = 12_500
  BASIC_RATE_HIGHEST_VALUE = 7500
  HIGHER_RATE = 50_000
  HIGHER_RATE_HIGHEST_VALUE = 45_000
  ADDITIONAL_RATE = 150_000

  def income_for(year)
    total = 0

    year.months.each { |month| total += monthly_income_for(month) }

    total
  end

  def tax_for(year)
    calculate_tax_for(income_for(year))
  end

  def tax_paid_for(year)
    tax = 0

    year.months.each { |month| tax += month.paye_incomes.sum(:vat) }

    tax
  end

  def net_for(year)
    income_for(year) - tax_for(year)
  end

  def reclaimable_tax_for(year)
    tax_rebate = 0.0

    year.months.each do |m|
      m.expenses.each do |e|
        tax_rebate += e.vat if e.reclaim_tax
      end
    end

    tax_rebate
  end

  def payable_tax_for(year)
    tax = tax_for(year) - reclaimable_tax_for(year) - tax_paid_for(year)

    return 0 if tax.negative?

    tax
  end

  private

  def monthly_income_for(month)
    month.incomes.sum(:balance) + month.paye_incomes.sum(:balance)
  end

  def calculate_tax_for(income)
    return additional_rate(income) if income > ADDITIONAL_RATE
    return higher_rate(income) if income > HIGHER_RATE
    return basic_rate(income) if income > BASIC_RATE

    0
  end

  def basic_rate(income)
    puts 'hit basic rate'
    (income - 12_500) * 0.2
  end

  def higher_rate(income)
    puts 'hit higher rate'
    (income - HIGHER_RATE) * 0.4 + BASIC_RATE_HIGHEST_VALUE
  end

  def additional_rate(income)
    puts 'hit additional rate'
    (income - ADDITIONAL_RATE) * 0.45 + BASIC_RATE_HIGHEST_VALUE + HIGHER_RATE_HIGHEST_VALUE
  end
end
