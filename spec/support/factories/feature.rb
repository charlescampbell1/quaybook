# frozen_string_literal: true

FactoryBot.define do
  factory :feature do
    feature { 'feature name' }
    code_id { 'fn' }
  end
end
