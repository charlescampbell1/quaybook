# frozen_string_literal: true

class Feature < ApplicationRecord
  has_and_belongs_to_many :accounts

  validates_presence_of :feature, :code_id
end
