# frozen_string_literal: true

class FeaturesController < ApplicationController
  before_action :authenticate_user!, :authenticate_admin!, :active_user?
  before_action :set_feature, only: %i[show edit update destroy enable disable]
  before_action :set_account, only: %i[enable disable]

  def index
    @features = Feature.all

    render 'components/pages/features/index'
  end

  def new
    @feature = Feature.new

    render 'components/pages/features/new'
  end

  def create
    @feature = Feature.new(feature_params)

    respond_to do |format|
      if @feature.save
        format.html { redirect_to show_feature_path(@feature), notice: 'Feature created.' }
      else
        format.html { redirect_to features_path, alert: 'Failed to create feature. Check logs.' }
      end
    end
  end

  def show
    @accounts = Account.all

    render 'components/pages/features/show'
  end

  def enable
    @account.features << @feature

    redirect_to show_account_path(@account), notice: "#{@feature.feature} enabled for #{@account.name}"
  end

  def disable
    @account.features.delete(@feature)

    redirect_to show_account_path(@account), notice: "#{@feature.feature} disabled for #{@account.name}"
  end

  def destroy
    @feature.destroy

    respond_to { |format| format.html { redirect_to features_path, notice: 'Feature deleted.' } }
  end

  private

  def set_feature
    @feature = Feature.find(params[:id])
  end

  def set_account
    @account = Account.find(params[:account_id])
  end

  def feature_params
    params.require(:feature).permit(:feature, :code_id)
  end
end
