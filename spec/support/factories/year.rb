# frozen_string_literal: true

FactoryBot.define do
  factory :year do
    year { '2020/21' }
  end
end
