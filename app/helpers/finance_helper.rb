# frozen_string_literal: true

module FinanceHelper
  def balance(data)
    return "<td class='negative-balance'>-£#{data.balance}</td>".html_safe if expense?(data)

    "<td class='positive-balance'>+£#{data.balance}</td>".html_safe
  end

  def total_income_for(month)
    month.incomes.sum(:balance) + month.paye_incomes.sum(:balance)
  end

  def total_expense_for(month)
    month.expenses.sum(:balance)
  end

  def month_to_date_balance_for(month)
    total_income_for(month) - total_expense_for(month)
  end

  def payable_vat_for(month)
    month.incomes.sum(:vat)
  end

  def reclaimable_vat_for(month)
    vat = 0

    month.expenses.each do |e|
      vat += e.vat if e.reclaim_tax
    end

    vat
  end

  def vat_already_paid_for(month)
    month.paye_incomes.sum(:vat)
  end

  def financial_ytd(year)
    data = {}

    year.months.each { |month| data[month.month[0..-4].to_sym] = month_to_date_balance_for(month) }

    data
  end

  private

  def income?(data)
    return true if data.class.to_s.include?('Income')

    false
  end

  def expense?(data)
    return true if income?(data).eql?(false)
  end
end
