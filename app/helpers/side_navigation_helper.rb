# frozen_string_literal: true

module SideNavigationHelper
  def side_navigation_profile_link(initial, title, path, active)
    link_to "<span class='dropped-nav'>#{initial}</span><span class='dropped-nav desktop-nav'>#{title}".html_safe,
            path, class: "link-item #{active?(active)}"
  end

  def side_navigation_profile_dropdown?
    return 'display' if active?('profile').eql?('active') || active?('switch').eql?('active')

    ''
  end

  def version
    "v#{File.read('./VERSION')}"
  end

  private

  def active?(page, alter = 'nope')
    return 'active' if request.original_url.to_s.include?(page.downcase)
    return 'active' if request.original_url.to_s.include?(alter&.downcase)

    ''
  end
end
