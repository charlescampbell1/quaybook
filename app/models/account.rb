# frozen_string_literal: true

class Account < ApplicationRecord
  has_and_belongs_to_many :users
  has_and_belongs_to_many :years
  has_and_belongs_to_many :invoices
  has_and_belongs_to_many :paye_incomes
  has_and_belongs_to_many :features

  validates_presence_of :name, :street_address, :city, :county, :postcode, :contact_number, :email

  def self.search(search)
    if search
      where('LOWER(name) LIKE ?', "%#{search.downcase}%")
    else
      where(nil)
    end
  end
end
