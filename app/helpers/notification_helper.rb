# frozen_string_literal: true

module NotificationHelper
  def notify(text:, category:)
    notification = Notification.create(notification: text, category: category, sender: current_user.full_name)

    selected_account.users.each do |user|
      user.notifications << notification
    end
  end

  def tr(notification)
    return "<tr class='unread-notification'>".html_safe unless notification.read

    '<tr>'.html_safe
  end

  def unread_notifications
    current_user.notifications.where(read: false).order('created_at DESC')
  end

  private

  def requires_action
    '<span class="badge badge-danger">ACTION REQUIRED</span>'.html_safe
  end

  def approved
    '<span class="badge badge-success">APPROVED</span>'.html_safe
  end
end
