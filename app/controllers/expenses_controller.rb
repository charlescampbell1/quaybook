# frozen_string_literal: true

class ExpensesController < ApplicationController
  before_action :authenticate_user!, :active_user?, :active_account?
  before_action :set_expense, only: %i[edit update destroy]

  def new
    @expense = Expense.new

    month = Month.find(params[:month_id])
    @month = month.month

    render 'components/pages/expense/new'
  end

  def create
    month = Month.find(params[:expense][:month_id])

    @expense = Expense.new(expense_params)

    if @expense.vat_exempt
      @expense.vat = 0
      @expense.reclaim_tax = false
    else
      @expense.vat = @expense.balance * 0.2
    end

    respond_to do |format|
      if @expense.save
        month.expenses << @expense
        format.html { redirect_to show_month_path(month), notice: 'Record added successfully' }
      else
        format.html { redirect_to show_month_path(month), alert: "Error: #{@expense.errors.to_a}" }
      end
    end
  end

  def edit
    render 'components/pages/expense/edit'
  end

  def update
    month = @expense.months.first

    respond_to do |format|
      if @expense.update(expense_params)
        if @expense.vat_exempt
          @expense.vat = 0
          @expense.reclaim_tax = false
        else
          @expense.vat = @expense.balance * 0.2
        end

        @expense.save
        format.html { redirect_to show_month_path(month), notice: 'The record updated successfully' }
      else
        format.html { redirect_to show_month_path(month), alert: "Error: #{@expense.errors.to_a}" }
      end
    end
  end

  def destroy
    month = @expense.months.first.id

    @expense.destroy

    respond_to do |format|
      format.html { redirect_to show_month_path(month), notice: 'Record deleted successfully' }
    end
  end

  private

  def set_expense
    @expense = Expense.find(params[:id])
  end

  def expense_params
    params.require(:expense).permit(:date_of, :summary, :category, :balance, :reclaim_tax, :vat_exempt)
  end
end
