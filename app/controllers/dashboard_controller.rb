# frozen_string_literal: true

class DashboardController < ApplicationController
  before_action :authenticate_user!
  before_action :active_user?, :active_account?, except: %i[holding]

  def index
    @account = selected_account.name
    finances_year = Finances::FinancialYear.new(account: selected_account, date: Time.now)
    @year = finances_year.find_or_create

    render 'components/pages/dashboard/index'
  end

  def holding
    return redirect_to root_url if current_user.active

    render 'components/pages/user/inactive_holding'
  end
end
