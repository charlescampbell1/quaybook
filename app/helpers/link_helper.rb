# frozen_string_literal: true

module LinkHelper
  def nav_link(icon, title, path, alter = 'nope')
    link_to "<i class='material-icons'>#{icon}</i><span class='desktop-nav'>#{title}</span>".html_safe,
            path, class: "link-item #{active?(title, alter)}"
  end

  def modal_link_to(text, path, style: '')
    link_to text, path,
            {
              remote: true,
              'data-toggle': 'modal',
              'data-target': '#modal-window',
              class: "submit-button success-button #{style}"
            }
  end

  def edit_path_for(data)
    return edit_expense_path(data) if expense?(data)

    edit_income_path(data)
  end

  def destroy_path_for(data)
    return destroy_expense_path(data) if expense?(data)

    destroy_income_path(data)
  end

  private

  def active?(page, alter = 'nope')
    return 'active' if request.original_url.to_s.include?(page.downcase)
    return 'active' if request.original_url.to_s.include?(alter&.downcase)

    ''
  end

  def active_home?
    return 'active' if request.original_url.to_s == root_url

    ''
  end
end
