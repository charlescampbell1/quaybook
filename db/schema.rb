# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_03_16_185239) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name", null: false
    t.string "street_address", null: false
    t.string "city", null: false
    t.string "county", null: false
    t.string "postcode", null: false
    t.string "contact_number", null: false
    t.string "email", null: false
    t.string "notes"
    t.boolean "active", default: true, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_accounts_on_user_id"
  end

  create_table "accounts_features", id: false, force: :cascade do |t|
    t.bigint "account_id", null: false
    t.bigint "feature_id", null: false
  end

  create_table "accounts_invoices", id: false, force: :cascade do |t|
    t.bigint "account_id", null: false
    t.bigint "invoice_id", null: false
    t.index ["account_id", "invoice_id"], name: "index_accounts_invoices_on_account_id_and_invoice_id"
    t.index ["invoice_id", "account_id"], name: "index_accounts_invoices_on_invoice_id_and_account_id"
  end

  create_table "accounts_paye_incomes", id: false, force: :cascade do |t|
    t.bigint "account_id", null: false
    t.bigint "paye_income_id", null: false
    t.index ["account_id", "paye_income_id"], name: "index_accounts_paye_incomes_on_account_id_and_paye_income_id"
    t.index ["paye_income_id", "account_id"], name: "index_accounts_paye_incomes_on_paye_income_id_and_account_id"
  end

  create_table "accounts_users", id: false, force: :cascade do |t|
    t.bigint "account_id", null: false
    t.bigint "user_id", null: false
    t.index ["account_id", "user_id"], name: "index_accounts_users_on_account_id_and_user_id"
    t.index ["user_id", "account_id"], name: "index_accounts_users_on_user_id_and_account_id"
  end

  create_table "accounts_years", id: false, force: :cascade do |t|
    t.bigint "account_id", null: false
    t.bigint "year_id", null: false
    t.index ["account_id", "year_id"], name: "index_accounts_years_on_account_id_and_year_id"
    t.index ["year_id", "account_id"], name: "index_accounts_years_on_year_id_and_account_id"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "expenses", force: :cascade do |t|
    t.date "date_of", null: false
    t.string "summary", null: false
    t.string "category", null: false
    t.decimal "balance", null: false
    t.boolean "reclaim_tax", default: false, null: false
    t.boolean "vat_exempt", default: false, null: false
    t.decimal "vat", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "expenses_months", id: false, force: :cascade do |t|
    t.bigint "expense_id", null: false
    t.bigint "month_id", null: false
    t.index ["expense_id", "month_id"], name: "index_expenses_months_on_expense_id_and_month_id"
    t.index ["month_id", "expense_id"], name: "index_expenses_months_on_month_id_and_expense_id"
  end

  create_table "features", force: :cascade do |t|
    t.string "feature", null: false
    t.string "code_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "incomes", force: :cascade do |t|
    t.date "date_of", null: false
    t.string "summary", null: false
    t.string "category", null: false
    t.decimal "balance", null: false
    t.decimal "vat", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "incomes_months", id: false, force: :cascade do |t|
    t.bigint "income_id", null: false
    t.bigint "month_id", null: false
    t.index ["income_id", "month_id"], name: "index_incomes_months_on_income_id_and_month_id"
    t.index ["month_id", "income_id"], name: "index_incomes_months_on_month_id_and_income_id"
  end

  create_table "invoices", force: :cascade do |t|
    t.string "title", null: false
    t.string "category"
    t.string "customer_name", null: false
    t.string "customer_contact_number", null: false
    t.string "notes"
    t.date "issued_on", null: false
    t.date "due_by", null: false
    t.date "paid_on"
    t.decimal "balance", default: "0.0", null: false
    t.decimal "vat", default: "0.0", null: false
    t.string "raised_by", null: false
    t.boolean "paid", default: false, null: false
    t.boolean "approved", default: false, null: false
    t.boolean "pending_approval", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "invoices_items", id: false, force: :cascade do |t|
    t.bigint "invoice_id", null: false
    t.bigint "item_id", null: false
    t.index ["invoice_id", "item_id"], name: "index_invoices_items_on_invoice_id_and_item_id"
    t.index ["item_id", "invoice_id"], name: "index_invoices_items_on_item_id_and_invoice_id"
  end

  create_table "items", force: :cascade do |t|
    t.string "description"
    t.decimal "balance"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "months", force: :cascade do |t|
    t.string "month"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "months_paye_incomes", id: false, force: :cascade do |t|
    t.bigint "month_id", null: false
    t.bigint "paye_income_id", null: false
    t.index ["month_id", "paye_income_id"], name: "index_months_paye_incomes_on_month_id_and_paye_income_id"
    t.index ["paye_income_id", "month_id"], name: "index_months_paye_incomes_on_paye_income_id_and_month_id"
  end

  create_table "months_years", id: false, force: :cascade do |t|
    t.bigint "month_id", null: false
    t.bigint "year_id", null: false
    t.index ["month_id", "year_id"], name: "index_months_years_on_month_id_and_year_id"
    t.index ["year_id", "month_id"], name: "index_months_years_on_year_id_and_month_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.string "notification", null: false
    t.string "category", null: false
    t.string "sender", null: false
    t.boolean "read", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "notifications_users", id: false, force: :cascade do |t|
    t.bigint "notification_id", null: false
    t.bigint "user_id", null: false
    t.index ["notification_id", "user_id"], name: "index_notifications_users_on_notification_id_and_user_id"
    t.index ["user_id", "notification_id"], name: "index_notifications_users_on_user_id_and_notification_id"
  end

  create_table "paye_incomes", force: :cascade do |t|
    t.date "date_of", null: false
    t.string "summary", null: false
    t.string "category", null: false
    t.decimal "balance", null: false
    t.decimal "vat", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "full_name", null: false
    t.string "contact_number"
    t.string "street_address"
    t.string "city"
    t.string "county"
    t.string "postcode"
    t.string "notes"
    t.boolean "active"
    t.string "status", default: "user", null: false
    t.string "selected_account"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "years", force: :cascade do |t|
    t.string "year"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
end
