class CreateIncomes < ActiveRecord::Migration[6.0]
  def change
    create_table :incomes do |t|
      t.date :date_of, null: false
      t.string :summary, null: false
      t.string :category, null: false
      t.decimal :balance, null: false
      t.decimal :vat, null: false

      t.timestamps
    end
  end
end
