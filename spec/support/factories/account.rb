# frozen_string_literal: true

FactoryBot.define do
  factory :account do
    name { Faker::Company.name }
    street_address { 'abc' }
    city { 'def' }
    county { 'ghi' }
    postcode { 'jkl' }
    contact_number { '123' }
    email { Faker::Internet.email }
    active { true }
  end
end
