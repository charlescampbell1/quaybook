class CreateJoinTableAccountsFeatures < ActiveRecord::Migration[6.0]
  def change
    create_join_table :accounts, :features do |t|
      # t.index [:account_id, :features_id]
      # t.index [:features_id, :account_id]
    end
  end
end
