# frozen_string_literal: true

class IncomeController < ApplicationController
  before_action :authenticate_user!, :active_user?, :active_account?
  before_action :set_income, only: %i[edit update destroy]

  def new
    @income = Income.new

    month = Month.find(params[:month_id])
    @month = month.month

    render 'components/pages/income/new'
  end

  def create
    month = Month.find(params[:income][:month_id])

    @income = Income.new(income_params)
    @income.vat = @income.balance * 0.2

    respond_to do |format|
      if @income.save
        month.incomes << @income
        format.html { redirect_to show_month_path(month), notice: 'Record added successfully' }
      else
        format.html { redirect_to show_month_path(month), alert: "Error: #{@income.errors.to_a}" }
      end
    end
  end

  def edit
    render 'components/pages/income/edit'
  end

  def update
    month = @income.months.first

    respond_to do |format|
      if @income.update(income_params)
        format.html { redirect_to show_month_path(month), notice: 'The record updated successfully' }
      else
        format.html { redirect_to show_month_path(month), alert: "Error: #{@income.errors.to_a}" }
      end
    end
  end

  def destroy
    month = @income.months.first.id

    @income.destroy

    respond_to do |format|
      format.html { redirect_to show_month_path(month), notice: 'Record deleted successfully' }
    end
  end

  private

  def set_income
    @income = Income.find(params[:id])
  end

  def income_params
    params.require(:income).permit(:date_of, :summary, :category, :balance)
  end
end
