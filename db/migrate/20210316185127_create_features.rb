class CreateFeatures < ActiveRecord::Migration[6.0]
  def change
    create_table :features do |t|
      t.string :feature, null: false
      t.string :code_id, null: false

      t.timestamps
    end
  end
end
