# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UsersController do
  login_admin

  describe '#index' do
    it 'renders the index template' do
      get :index

      expect(response).to render_template('index')
    end
  end

  describe '#edit' do
    it 'renders the edit template' do
      get :edit, params: { id: 1 }

      expect(response).to render_template('edit')
    end
  end

  describe '#update' do
    let(:new_name) { 'Moris Mos' }

    before { @user = create(:user) }

    context 'with valid attributes' do
      it 'updates the users fullname' do
        put :update, params: { id: @user.id, user: FactoryBot.attributes_for(:user, full_name: new_name) }

        @user.reload

        expect(@user.full_name).to eq(new_name)
      end
    end

    context 'with invalid attributes' do
      it 'does not update the users fullname' do
        put :update, params: { id: @user.id, user: FactoryBot.attributes_for(:user, full_name: nil) }

        @user.reload

        expect(@user.full_name).to eq(@user.full_name)
      end
    end
  end

  describe '#destroy' do
    it 'removes the user' do
      user = create(:user)

      expect { delete :destroy, params: { id: user.id } }.to change(User, :count).by(-1)
    end
  end

  describe '#profile' do
    it 'renders the index template' do
      get :profile

      expect(response).to render_template('profile/index')
    end
  end

  describe '#update_profile' do
    let(:new_name) { 'Moris Mos' }

    context 'with valid attributes' do
      it 'updates the users fullname' do
        put :update_profile, params: { user: FactoryBot.attributes_for(:user, full_name: new_name) }

        expect(controller.current_user.full_name).to eq(new_name)
      end
    end

    context 'with invalid attributes' do
      it 'does not update the users fullname' do
        put :update_profile, params: { user: FactoryBot.attributes_for(:user, full_name: nil) }

        expect(controller.current_user.full_name).to eq(controller.current_user.full_name)
      end
    end
  end

  describe '#profile_picture' do
    it 'renders the edit profile picture modal' do
      get :profile_picture, format: :js, xhr: true

      expect(response).to render_template('edit_profile_picture')
    end
  end

  describe '#remove_profile_picture' do
    it 'removes the users profile picture' do
      delete :remove_profile_picture

      expect(controller.current_user.avatar).not_to eq(nil)
    end
  end
end
