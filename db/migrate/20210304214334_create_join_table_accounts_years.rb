class CreateJoinTableAccountsYears < ActiveRecord::Migration[6.0]
  def change
    create_join_table :accounts, :years do |t|
      t.index [:account_id, :year_id]
      t.index [:year_id, :account_id]
    end
  end
end
