# frozen_string_literal: true

module AccountHelper
  def account_active?(account)
    if account.active == true
      '<span class="badge badge-success">active</span>'.html_safe
    else
      '<span class="badge badge-secondary">inactive</span>'.html_safe
    end
  end

  def current_selected_card(account)
    'current-selected-card' if selected_account == account
  end

  def account_status_for(account)
    if account.active == false
      '<a>' \
        '<i class="material-icons text-danger">warning</i>' \
        'This account has been disabled...' \
      '</a>'.html_safe
    else
      '<a><i class="material-icons text-success">done_all</i>Active</a>'
        .html_safe
    end
  end

  def map_year_to_account(year)
    selected_account.years << year
  end

  def map_invoice_to_account(invoice)
    selected_account.invoices << invoice
  end
end
