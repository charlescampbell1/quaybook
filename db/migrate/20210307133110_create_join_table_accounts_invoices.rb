class CreateJoinTableAccountsInvoices < ActiveRecord::Migration[6.0]
  def change
    create_join_table :accounts, :invoices do |t|
      t.index [:account_id, :invoice_id]
      t.index [:invoice_id, :account_id]
    end
  end
end
