# frozen_string_literal: true

class ItemsController < ApplicationController
  before_action :authenticate_user!, :active_user?, :active_account?
  before_action :set_item, except: %i[new create]

  def new
    @item = Item.new

    render 'components/pages/item/new'

    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    @item = Item.new(item_params)
    @invoice = Invoice.find(params[:item][:invoice_id])

    respond_to do |format|
      if @item.save
        @invoice.items << @item
        format.html { redirect_to show_invoice_path(@invoice), notice: 'Item Added' }
      else
        format.html { redirect_to show_invoice_path(@invoice), alert: "Error: #{@item.errors.to_a}" }
      end
    end
  end

  def edit
    render 'components/pages/item/edit'

    respond_to do |format|
      format.html
      format.js
    end
  end

  def update
    invoice = @item.invoices.first

    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to show_invoice_path(invoice), notice: 'Updated!' }
      else
        format.html { redirect_to show_invoice_path(invoice), alert: "Error: #{@item.errors.to_a}" }
      end
    end
  end

  def destroy
    invoice = @item.invoices.first.id

    @item.destroy

    respond_to do |format|
      format.html { redirect_to show_invoice_path(invoice), notice: 'Record deleted successfully' }
    end
  end

  private

  def set_item
    @item = Item.find(params[:id])
  end

  def item_params
    params.require(:item).permit(:description, :balance)
  end
end
