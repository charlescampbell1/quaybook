# frozen_string_literal: true

module ToolbarHelper
  def page_title
    return 'Dashboard' if url_contains?('dashboard')
    return 'Bookkeeping' if url_contains?('bookkeeping')
    return 'People' if url_contains?('people') || url_contains?('user')
    return 'Account' if url_contains?('account')
    return 'Support' if url_contains?('support')

    ''
  end

  private

  def url_contains?(text)
    return true if request.original_url.to_s.include?(text)

    false
  end
end
