class CreateExpenses < ActiveRecord::Migration[6.0]
  def change
    create_table :expenses do |t|
      t.date :date_of, null: false
      t.string :summary, null: false
      t.string :category, null: false
      t.decimal :balance, null: false
      t.boolean :reclaim_tax, null: false, default: false
      t.boolean :vat_exempt, null: false, default: false
      t.decimal :vat, null: false

      t.timestamps
    end
  end
end
