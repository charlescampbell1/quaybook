# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AccountsController do
  login_admin

  describe '#index' do
    before { get :index }

    it 'renders the index template' do
      expect(response).to render_template('index')
    end
  end

  describe '#new' do
    it 'assigns a new account to @account' do
      get :new

      expect(assigns(:account)).to be_a_new(Account)
    end
  end

  describe '#setup' do
    it 'assigns a new account to @account' do
      get :setup

      expect(assigns(:account)).to be_a_new(Account)
    end
  end

  describe '#create' do
    context 'when a user first creates an account' do
      logout_user
      login_user

      it 'creates the account and welcomes them to the application' do
        expect { post :create, params: { account: FactoryBot.attributes_for(:account) } }
          .to change(Account, :count).by(1)
      end
    end

    context 'with valid attributes' do
      it 'creates a new account' do
        expect { post :create, params: { account: FactoryBot.attributes_for(:account) } }
          .to change(Account, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      it 'does not create a new account' do
        expect { post :create, params: { account: FactoryBot.attributes_for(:account, name: nil) } }
          .to change(Account, :count).by(0)
      end
    end
  end

  describe '#show' do
    it 'displays the account information' do
      get :show, params: { id: 1 }

      expect(response).to render_template('show')
    end
  end

  describe '#edit' do
    it 'displays the account information' do
      get :edit, params: { id: 1 }

      expect(response).to render_template('edit')
    end
  end

  describe '#update' do
    let(:new_name) { 'Testing Company' }

    before { @account = create(:account) }

    context 'with valid attributes' do
      it 'updates the account name' do
        put :update, params: { id: @account.id, account: FactoryBot.attributes_for(:account, name: new_name) }

        @account.reload

        expect(@account.name).to eq(new_name)
      end
    end

    context 'with invalid attributes' do
      it 'does not update the account name' do
        put :update, params: { id: @account.id, account: FactoryBot.attributes_for(:account, name: nil) }

        @account.reload

        expect(@account.name).to eq(@account.name)
      end
    end
  end

  describe '#destroy' do
    it 'removes the account' do
      account = create(:account)

      expect { delete :destroy, params: { id: account.id } }.to change(Account, :count).by(-1)
    end
  end

  describe '#add_users' do
    it 'shows a list of all users not in the account' do
      account = create(:account)
      user = create(:user)

      get :add_users, params: { id: account.id }

      expect(assigns(:users)).to include(user)
    end
  end

  describe '#add_user' do
    it 'adds the user to the account' do
      account = create(:account)
      user = create(:user)

      get :add_user, params: { account_id: account.id, user_id: user.id }

      expect(account.users).to eq([user])
    end
  end

  describe '#remove_user' do
    it 'removes the user from the account' do
      account = create(:account)
      user = create(:user)

      get :remove_user, params: { account_id: account.id, user_id: user.id }

      expect(account.users).not_to include([user])
    end
  end

  describe '#switch' do
    it 'displays the list of currently registered accounts' do
      get :switch

      expect(response).to render_template('switch')
    end
  end

  describe '#switch_account' do
    context 'when the user does not have access to the account' do
      it 'does not switch account' do
        user = create(:user)

        get :switch_account, params: { id: 1 }

        expect(user.selected_account).to eq(nil)
      end
    end

    context 'when the user has access to the account' do
      it 'switches the account' do
        account = create(:account)

        controller.current_user.accounts << account

        get :switch_account, params: { id: account.id }

        expect(controller.current_user.selected_account).to eq(account.id.to_s)
      end
    end
  end
end
