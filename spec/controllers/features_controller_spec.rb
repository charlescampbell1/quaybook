# frozen_string_literal: true

require 'rails_helper'

RSpec.describe FeaturesController do
  login_admin

  describe '#index' do
    it 'renders the index template' do
      get :index

      expect(response).to render_template('index')
    end
  end

  describe '#new' do
    it 'assigns a new feature to @feature' do
      get :new

      expect(assigns(:feature)).to be_a_new(Feature)
    end
  end

  describe '#create' do
    context 'with valid attributes' do
      it 'creates a new account' do
        expect { post :create, params: { feature: FactoryBot.attributes_for(:feature) } }
          .to change(Feature, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      it 'does not create a new feature' do
        expect { post :create, params: { feature: FactoryBot.attributes_for(:feature, feature: nil) } }
          .to change(Feature, :count).by(0)
      end
    end
  end

  describe '#show' do
    it 'displays the feature information' do
      get :show, params: { id: 1 }

      expect(response).to render_template('show')
    end
  end

  describe '#enable' do
    it 'enables the feature for an account' do
      account = create(:account)
      feature = create(:feature)

      get :enable, params: { id: feature.id, account_id: account.id }

      expect(account.features).to include(feature)
    end
  end

  describe '#disable' do
    it 'disables the feature for an account' do
      account = create(:account)
      feature = create(:feature)

      get :enable, params: { id: feature.id, account_id: account.id }
      get :disable, params: { id: feature.id, account_id: account.id }

      expect(account.features).not_to include(feature)
    end
  end

  describe '#destroy' do
    it 'deletes the feature' do
      feature = create(:feature)

      expect { delete :destroy, params: { id: feature.id } }.to change(Feature, :count).by(-1)
    end
  end
end
