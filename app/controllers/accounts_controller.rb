# frozen_string_literal: true

class AccountsController < ApplicationController
  before_action :authenticate_user!, :active_user?
  before_action :active_account?, only: %i[switch]
  before_action :authenticate_admin!, only: %i[index destroy]
  before_action :set_account, only: %i[show edit update destroy add_users switch_account]

  def index
    @pagy, @accounts = pagy(Account.search(params[:search]).order(:name), items: 15)

    render 'components/pages/account/index'
  end

  def new
    @account = Account.new

    render 'components/pages/account/new'
  end

  def setup
    @account = Account.new

    render 'components/pages/account/setup'
  end

  def create
    @account = Account.new(account_params)
    @account.users << current_user

    respond_to do |format|
      if @account.save && current_user.status.eql?('admin')
        current_user.selected_account = @account.id
        current_user.save
        format.html { redirect_to show_account_path(@account), notice: "#{@account.name} has been created." }
      elsif @account.save
        current_user.selected_account = @account.id
        current_user.save
        format.html { redirect_to root_url, notice: 'Welcome to QuayBook' }
      else
        format.html { redirect_to new_account_path(@account), alert: "Error: #{@account.errors.to_a}" }
      end
    end
  end

  def show
    @users = @account.users
    @features = Feature.all

    render 'components/pages/account/show'
  end

  def edit
    @users = @account.users

    render 'components/pages/account/edit'
  end

  def update
    respond_to do |format|
      if @account.update(account_params)
        format.html { redirect_to show_account_path(@account), notice: 'Updated!' }
      else
        format.html { redirect_to show_account_path(@account), alert: "Error: #{@account.errors.to_a}" }
      end
    end
  end

  def destroy
    @account.destroy
    respond_to do |format|
      format.html { redirect_to accounts_path, notice: 'Account was successfully destroyed.' }
    end
  end

  def add_users
    account = Account.find(params[:id])

    @users = User.all - account.users

    render 'components/pages/account/add_user'
  end

  def add_user
    account = Account.find(params[:account_id])
    user = User.find(params[:user_id])

    account.users << user

    respond_to do |format|
      format.html do
        redirect_to show_account_path(account), notice: "#{user.full_name} has been added to #{account.name}"
      end
    end
  end

  def remove_user
    account = Account.find(params[:account_id])
    user = User.find(params[:user_id])

    user.accounts.delete(account)

    respond_to do |format|
      format.html { redirect_to edit_account_path(account), notice: "#{user.full_name} removed from #{account.name}" }
    end
  end

  def switch
    @accounts = current_user.accounts

    render 'components/pages/account/switch'
  end

  def switch_account
    if @account.users.include?(current_user)
      current_user.selected_account = @account.id
      redirect_to dashboard_path, notice: 'Account Switched' if current_user.save
    else
      redirect_to root_url, alert: 'You do not have access to that account.'
    end
  end

  private

  def set_account
    @account = Account.find(params[:id])
  end

  def account_params
    params.require(:account).permit(
      :name, :street_address, :city, :county, :postcode, :contact_number, :email, :active, :notes
    )
  end
end
