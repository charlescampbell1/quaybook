# frozen_string_literal: true

class CreateJoinTableAccountsUsers < ActiveRecord::Migration[6.0]
  def change
    create_join_table :accounts, :users do |t|
      t.index %i[account_id user_id]
      t.index %i[user_id account_id]
    end
  end
end
