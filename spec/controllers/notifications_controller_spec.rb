# frozen_string_literal: true

require 'rails_helper'

RSpec.describe NotificationsController do
  login_user

  describe '#index' do
    it 'renders the index template' do
      get :index

      expect(response).to render_template('index')
    end
  end

  describe '#read' do
    it 'marks the notification as read' do
      notification = create(:notification)

      get :read, params: { id: notification.id }

      expect(notification.read) == true
    end
  end

  describe '#read_all' do
    it 'marks all notifications as read' do
      notification = create(:notification)

      controller.current_user.notifications << notification

      get :read_all

      expect(notification.read).to eq(true)
    end
  end
end
