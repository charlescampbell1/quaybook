# frozen_string_literal: true

class InvoicesController < ApplicationController
  include AccountHelper
  include InvoicesHelper
  include NotificationHelper

  before_action :authenticate_user!, :active_user?, :active_account?
  before_action :set_invoice, except: %i[index new create]

  def index
    @pagy, @invoices = pagy(account_invoices.search(params[:search]).order('due_by DESC'), items: 15)

    render 'components/pages/invoice/index'
  end

  def new
    @invoice = Invoice.new

    render 'components/pages/invoice/new'
  end

  def create
    @invoice = Invoice.new(invoice_params)
    @invoice.raised_by = current_user.id

    respond_to do |format|
      if @invoice.save
        map_invoice_to_account(@invoice)
        notify(text: "Invoice #{@invoice.id} has been raised.", category: 'Invoice')
        format.html { redirect_to show_invoice_path(@invoice), notice: 'Invoice created' }
      else
        format.html { redirect_to invoice_path, alert: "Error: #{@invoice.errors.to_a}" }
      end
    end
  end

  def edit
    return redirect_to show_invoice_path(@invoice), alert: 'This invoice has been approved.' if @invoice.approved

    render 'components/pages/invoice/edit'
  end

  def update
    respond_to do |format|
      if @invoice.update(invoice_params)
        format.html { redirect_to show_invoice_path(@invoice), notice: 'Updated!' }
      else
        format.html { redirect_to show_invoice_path(@invoice), alert: "Error: #{@invoice.errors.to_a}" }
      end
    end
  end

  def show
    @invoice.balance = @invoice.items.sum(:balance)
    @invoice.vat = @invoice.balance * 0.2
    @invoice.save

    render 'components/pages/invoice/show'
  end

  def request_approval
    @invoice.pending_approval = true
    @invoice.save

    notify(text: "Invoice #{@invoice.id} needs approval.", category: 'Invoice')

    redirect_to show_invoice_path(@invoice)
  end

  def approve
    if @invoice.items.empty?
      redirect_to show_invoice_path(@invoice), alert: 'You need to have items on an invoice to approve.'
    else
      @invoice.approved = true
      notify(text: "Invoice #{@invoice.id} has been approved.", category: 'Invoice')

      redirect_to show_invoice_path(@invoice) if @invoice.save
    end
  end

  def reject
    @invoice.approved = false

    notify(text: "Invoice #{@invoice.id} has been declined.", category: 'Invoice')

    redirect_to show_invoice_path(@invoice) if @invoice.save
  end

  def pay
    render 'components/pages/invoice/pay'

    respond_to do |format|
      format.html
      format.js
    end
  end

  def paid
    @invoice.paid_on = params[:invoice][:paid_on]
    @invoice.paid = true

    notify(text: "Invoice #{@invoice.id} paid.", category: 'Invoice')

    record = Finances::Invoice.new(user: current_user, invoice: @invoice)
    record.add_income_record if @invoice.save

    redirect_to show_invoice_path(@invoice), notice: 'A record has been created in your books.'
  end

  def destroy
    @invoice.destroy

    respond_to { |format| format.html { redirect_to invoice_path, notice: 'Invoice deleted' } }
  end

  private

  def set_invoice
    @invoice = Invoice.find(params[:id])
  end

  def invoice_params
    params.require(:invoice)
          .permit(:customer_name, :customer_contact_number, :issued_on, :due_by, :balance, :notes, :title, :category)
  end
end
