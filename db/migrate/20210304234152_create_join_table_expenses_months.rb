class CreateJoinTableExpensesMonths < ActiveRecord::Migration[6.0]
  def change
    create_join_table :expenses, :months do |t|
      t.index [:expense_id, :month_id]
      t.index [:month_id, :expense_id]
    end
  end
end
