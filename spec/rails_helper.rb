# frozen_string_literal: true

require 'simplecov'
require 'simplecov-console'

SimpleCov.start 'rails'
SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter.new(
  [
    SimpleCov::Formatter::HTMLFormatter
    # SimpleCov::Formatter::Console
  ]
)

require 'spec_helper'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../config/environment', __dir__)

# Eager load all files so that SimpleCov reports accurate coverage.
Rails.application.eager_load!

require 'support/factory_bot'
require 'shoulda/matchers'
require 'devise'
require 'faker'
require_relative 'support/controller_macros'

require 'rspec/rails'

begin
  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end

RSpec.configure do |config|
  config.include Devise::Test::ControllerHelpers, type: :controller
  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.use_transactional_fixtures = false
  config.infer_spec_type_from_file_location!
  config.filter_rails_from_backtrace!
  config.extend ControllerMacros, type: :controller
end
