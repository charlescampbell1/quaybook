# frozen_string_literal: true

class Income < ApplicationRecord
  has_and_belongs_to_many :months
end
