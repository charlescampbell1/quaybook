class CreateJoinTableAccountsPayeIncomes < ActiveRecord::Migration[6.0]
  def change
    create_join_table :accounts, :paye_incomes do |t|
      t.index [:account_id, :paye_income_id]
      t.index [:paye_income_id, :account_id]
    end
  end
end
