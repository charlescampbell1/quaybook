# frozen_string_literal: true

module BookkeepingHelper
  def year_selector
    years = []

    year = current_year

    while year >= current_year - 5
      years << "#{year}/#{year - 1999}"
      year -= 1
    end

    years
  end

  def year_exists?(year)
    return false if account_years(year.year).empty?

    true
  end

  def generate_months_for(year)
    i = 0

    while i < months.count
      month = Month.create(month: format_month(i, start_year(year)))

      year.months << month

      i += 1
    end
  end

  def stat(text)
    "<h6 class='subtitle-grey small-text'>#{text}</h6>".html_safe
  end

  def stat_title(text)
    "<h6 class='title-grey small-text'>#{text}</h6>".html_safe
  end

  def date_value_for(month)
    month = Month.find(month)

    month_number = format '%02d', Date::MONTHNAMES.index(month.month.gsub(/\s.+/, ''))
    year = month.month.chars.last(2).join

    "20#{year}-#{month_number}-01"
  end

  private

  def current_year
    Time.now.year
  end

  def start_year(year)
    year.year[2..3]
  end

  def account_years(year)
    selected_account.years.where(year: year)
  end

  def months
    %w[April May June July August September October November December January February March April]
  end

  def format_month(counter, start_year)
    "#{months[counter]} '#{set_year_format(start_year, counter)}"
  end

  def set_year_format(year, counter)
    return year if counter < 9

    (year.to_i + 1).to_s
  end
end
