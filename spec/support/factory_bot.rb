# frozen_string_literal: true

require 'factory_bot'
require_relative 'factories/user'
require_relative 'factories/account'
require_relative 'factories/notification'
require_relative 'factories/feature'
require_relative 'factories/year'

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
end
