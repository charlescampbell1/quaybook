# frozen_string_literal: true

class Invoice < ApplicationRecord
  has_and_belongs_to_many :accounts
  has_and_belongs_to_many :items

  # rubocop:disable Lint/DuplicateBranch
  def self.search(search)
    if search == ''
      where(nil)
    elsif search
      where(id: search)
    else
      where(nil)
    end
  end
  # rubocop:enable Lint/DuplicateBranch
end
