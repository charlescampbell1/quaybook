# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    full_name { Faker::Name.name }
    email { Faker::Internet.email }
    contact_number { Faker::PhoneNumber.phone_number }
    password { 'password' }
    password_confirmation { 'password' }
    status { 'user' }
    active { true }
  end
end
