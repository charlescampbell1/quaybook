# Contributing

Small document on how to contribute towards quaybooks.

---

## Development

### Local Development

1. Clone this repository to your local machine
2. Run `bundle install` to install the applications dependencies
3. Run `bin/rails server` to start the app at 'http://localhost:3000'
4. Run `bundle exec rspec` to run the ruby test suite
5. Run `bundle exec rubocop` to run the lint tool
6. Run `bundle exec rails console` to boot up the console
7. Finally write some code!

### Dockerized Development

1. _TODO: Start up a development server inside a docker container `dev/server.sh`_
2. _TODO: Run rubocop inside a docker container `dev/lint.sh`_
3. _TODO: Run rspec inside a docker container `dev/test.sh`_

### Migrations

1. To create a join table run the following: `rails g migration CreateJoinTableAccountsUsers accounts users`

---

## Release

### Conventional Commits

This project uses standard-version to generate a changelog, so it's important
that in order for this to be generated correctly the commits follow the
conventional commit guidelines. Detailed here: www.conventionalcommits.org.

The main prefixes are:

- `fix` : Any small fix to the code base
- `refactor` : Refactoring of code
- `feat` : A finished feature with full functionality
- `docs` : Documentation changes
- `test` : Any form of testing
- `lint` : Rubocop fixes

### Generating a new version

Use standard version as described above to generate the changelog in preparation
for release.

1. Branch a `release` branch off master.
2. Run `make new-version`.
3. Raise PR for new release.
