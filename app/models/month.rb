# frozen_string_literal: true

class Month < ApplicationRecord
  has_and_belongs_to_many :years
  has_and_belongs_to_many :incomes
  has_and_belongs_to_many :expenses
  has_and_belongs_to_many :paye_incomes
end
