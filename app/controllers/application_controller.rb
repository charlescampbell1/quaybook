# frozen_string_literal: true

# :nocov:
class ApplicationController < ActionController::Base
  include Pagy::Backend

  before_action :configure_permitted_parameters, if: :devise_controller?
  helper_method :selected_account, :admin?

  def authenticate_admin!
    return if current_user.status.eql?('admin')

    redirect_to root_url, alert: 'You do not have permission to view that page.'
  end

  def active_user?
    return if current_user.active

    redirect_to holding_path
  end

  def active_account?
    return unless current_user.accounts.empty?

    redirect_to account_setup_path, notice: 'Welcome! Before using QuayBooks, you will need an Account'
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) do |u|
      u.permit(
        :email,
        :password,
        :full_name,
        :contact_number,
        :street_address,
        :city,
        :county,
        :postcode,
        :notes,
        :active,
        :status
      )
    end

    devise_parameter_sanitizer.permit(:account_update) do |u|
      u.permit(
        :email,
        :password,
        :full_name,
        :contact_number,
        :street_address,
        :city,
        :county,
        :postcode,
        :notes,
        :active,
        :status
      )
    end
  end

  private

  def selected_account
    account = Account.find_by(id: current_user.selected_account)

    return current_user.accounts.first if account.nil?
    return account if current_user.selected_account

    current_user.accounts.first
  end

  def admin?
    return true if current_user.status.eql?('admin')

    false
  end
end
