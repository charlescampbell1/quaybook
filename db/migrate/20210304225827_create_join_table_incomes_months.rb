class CreateJoinTableIncomesMonths < ActiveRecord::Migration[6.0]
  def change
    create_join_table :incomes, :months do |t|
      t.index [:income_id, :month_id]
      t.index [:month_id, :income_id]
    end
  end
end
