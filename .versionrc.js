module.exports = config = {};

config.bumpFiles = [{ filename: "VERSION", type: "plain-text" }];

const host = "https://gitlab.com/charlescampbell1/quaybook";

config.commitUrlFormat = `${host}/-/commit/{{hash}}`;

config.compareUrlFormat = `${host}/-/compare/{{previousTag}}...{{currentTag}}`;
