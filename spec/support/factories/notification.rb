# frozen_string_literal: true

FactoryBot.define do
  factory :notification do
    notification { 'note' }
    category { 'invoice' }
    sender { 'user' }
    read { false }
  end
end
