# frozen_string_literal: true

class PayeIncomesController < ApplicationController
  before_action :authenticate_user!, :active_user?, :active_account?, :paye_enabled?
  before_action :set_paye_income, only: %i[edit update destroy]

  include FeatureHelper

  def index
    @paye_incomes = selected_account.paye_incomes.order('date_of DESC')

    @pagy, @paye_incomes = pagy(selected_account.paye_incomes.order('date_of DESC'), items: 15)

    render 'components/pages/paye_income/index'
  end

  def new
    @paye_income = PayeIncome.new

    render 'components/pages/paye_income/new'
  end

  def create
    @paye_income = PayeIncome.new(paye_income_params)
    @paye_income.category = 'PAYE Income'

    finances_year = Finances::FinancialYear.new(account: selected_account, date: @paye_income.date_of)
    year = finances_year.find_or_create
    month = year.months.where(month: @paye_income.date_of.strftime("%B '%y")).first

    respond_to do |format|
      if @paye_income.save
        selected_account.paye_incomes << @paye_income

        insert_bookkeeping_record_to(month)

        format.html { redirect_to paye_income_path, notice: 'Income record created.' }
      else
        format.html { redirect_to invoice_path, alert: "Error: #{@paye_income.errors.to_a}" }
      end
    end
  end

  def insert_bookkeeping_record_to(month)
    if @paye_income.date_of.strftime('%B') == 'April' && @paye_income.date_of.strftime('%d').to_i >= 6
      finances_year = Finances::FinancialYear.new(account: selected_account, date: @paye_income.date_of + 1.year)
      year = finances_year.find_or_create
      month = year.months.where(month: @paye_income.date_of.strftime("%B '%y")).first
    end

    month.paye_incomes << @paye_income
  end

  def edit
    render 'components/pages/paye_income/edit'
  end

  def update
    respond_to do |format|
      if @paye_income.update(paye_income_params)
        format.html { redirect_to paye_income_path, notice: 'Income record updated.' }
      else
        format.html { redirect_to invoice_path, alert: "Error: #{@paye_income.errors.to_a}" }
      end
    end
  end

  def destroy
    @paye_income.destroy

    respond_to { |format| format.html { redirect_to paye_income_path, notice: 'Record deleted.' } }
  end

  private

  def set_paye_income
    @paye_income = PayeIncome.find(params[:id])
  end

  def paye_income_params
    params.require(:paye_income).permit(:date_of, :summary, :balance, :vat)
  end

  def paye_enabled?
    redirect_to root_url, alert: 'You do not have access to this feature.' unless feature_enabled?('paye_income')
  end
end
