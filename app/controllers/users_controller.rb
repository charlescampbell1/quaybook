# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_admin!, except: %i[profile update_profile]
  before_action :set_user, only: %i[edit update destroy]

  def index
    @pagy, @people = pagy(User.search(params[:search]).order(:full_name), items: 15)

    render 'components/pages/user/index'
  end

  def edit
    @accounts = @user.accounts

    render 'components/pages/user/edit'
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to edit_user_path(@user), notice: "#{@user.full_name}'s details updated successfully." }
      else
        format.html { redirect_to edit_user_path(@user), alert: "Error: #{@user.errors.to_a}" }
      end
    end
  end

  def destroy
    full_name = @user.full_name

    @user.destroy

    respond_to { |format| format.html { redirect_to users_path, notice: "#{full_name} has been removed." } }
  end

  def profile
    render 'components/pages/profile/index'
  end

  def update_profile
    respond_to do |format|
      if current_user.update(user_params)
        format.html { redirect_to profile_path, notice: 'Profile Updated!' }
      else
        format.html { redirect_to profile_path, alert: 'Something went wrong.' }
      end
    end
  end

  def profile_picture
    render 'components/pages/profile/edit_profile_picture'
    respond_to do |format|
      format.html
      format.js
    end
  end

  def remove_profile_picture
    current_user.avatar = nil
    current_user.save

    redirect_to profile_path, notice: 'Profile Updated!'
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(
      :full_name, :email, :contact_number, :street_address, :city, :county, :postcode, :notes, :holiday, :password,
      :status, :active, :avatar
    )
  end
end
