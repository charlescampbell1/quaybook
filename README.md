# README

## Overview

Final year project for Bournemouth University - Software Engineering. A business management application that primarily
focuses on bookkeeping and staff management. The original idea came from when I wanted to make a tool that made
bookkeeping for a family friend easier, then the requirement just grew.

## Documents

A list of the other markdown documents worth reading in this repositiory.

- FEATURE_FLAG.md - provides detail on how to add and remove features
- CONTRIBUTING.md - provides detail on how to contribute
- CHANGELOG.md - shows all changes based on commit history
