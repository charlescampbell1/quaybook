# config valid for current version and patch releases of Capistrano
lock "~> 3.16.0"

set :application, 'quaybook'
set :repo_url, 'git@gitlab.com:charlescampbell1/quaybook.git'
set :deploy_to, "/home/deploy/#{fetch :application}"
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets',
       'vendor/bundle', '.bundle', 'public/system', 'public/uploads'

set :keep_releases, 3
