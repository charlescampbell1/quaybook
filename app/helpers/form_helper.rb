# frozen_string_literal: true

module FormHelper
  def textfield(form:, attribute:, label:, size:, placeholder: nil, required: nil, field: 'text', disabled: nil,
                pattern: nil)
    render 'components/atoms/form/textfield', form: form, attribute: attribute, label: label, field: field,
                                              placeholder: placeholder, required: required, size: size,
                                              disabled: disabled, pattern: pattern
  end

  def selectfield(form:, attribute:, label:, options:, required:, size:, disabled: nil)
    render 'components/atoms/form/selectfield', form: form, attribute: attribute, label: label, options: options,
                                                required: required, size: size, disabled: disabled
  end

  def checkbox(form:, attribute:, label:, required:, size:, disabled: nil)
    render 'components/atoms/form/checkbox', form: form, attribute: attribute, label: label, size: size,
                                             required: required, disabled: disabled
  end

  def textarea(form:, attribute:, label:, required:, size:, placeholder: nil, rows: 5, disabled: nil)
    render 'components/atoms/form/textarea', form: form, attribute: attribute, label: label, placeholder: placeholder,
                                             required: required, size: size, rows: rows, disabled: disabled
  end

  def datepicker(form:, attribute:, label:, required:, size:, value: nil, disabled: nil)
    render 'components/atoms/form/datepicker', form: form, attribute: attribute, label: label, required: required,
                                               size: size, value: value, disabled: disabled
  end
end
