# frozen_string_literal: true

module LayoutHelper
  def column_title(heading:, icon:)
    render 'components/atoms/titles/column', heading: heading, icon: icon
  end
end
