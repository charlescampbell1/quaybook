# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AccountHelper do
  describe '#account_active?' do
    context 'when the account is active' do
      it 'returns an active badge' do
        account = create(:account)

        expect(helper.account_active?(account)).to eq('<span class="badge badge-success">active</span>')
      end
    end

    context 'when the account is inactive' do
      it 'returns an inactive badge' do
        account = create(:account, active: false)

        expect(helper.account_active?(account)).to eq('<span class="badge badge-secondary">inactive</span>')
      end
    end
  end

  describe '#account_status_for' do
    context 'when the account is inactive' do
      it 'returns an account disabled label' do
        account = create(:account, active: false)

        expect(helper.account_status_for(account))
          .to eq('<a><i class="material-icons text-danger">warning</i>This account has been disabled...</a>')
      end
    end

    context 'when the account is active' do
      it 'returns an active label' do
        account = create(:account)

        expect(helper.account_status_for(account))
          .to eq('<a><i class="material-icons text-success">done_all</i>Active</a>')
      end
    end
  end
end
