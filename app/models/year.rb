# frozen_string_literal: true

class Year < ApplicationRecord
  has_and_belongs_to_many :accounts
  has_and_belongs_to_many :months

  validates_presence_of :year
end
