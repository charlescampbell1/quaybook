# frozen_string_literal: true

module UserHelper
  def navigation_avatar
    return image_tag current_user.avatar if current_user.avatar.attached?

    image_tag user_initial(current_user)
  end

  def profile_avatar(user)
    return image_tag current_user.avatar, class: 'card-profile-picture' if current_user.avatar.attached?

    image_tag user_initial(user), class: 'card-profile-picture'
  end

  def user_status?(user)
    return '<span class="badge badge-warning">ADMIN</span>' if user.status.eql?('admin')
    return '<span class="badge badge-success">OWNER</span>' if user.status.eql?('owner')
    return '<span class="badge badge-secondary">ACCOUNTANT</span>' if user.status.eql?('accountant')

    '<span class="badge badge-info">USER</span>'
  end

  def user_active?(user)
    return '<span class="badge badge-success">ACTIVE</span>' if user.active

    '<span class="badge badge-secondary">DISABLED</span>'
  end

  def user_status_options
    %w[admin accountant owner user]
  end

  def last_login
    current_user.last_sign_in_at.strftime('%H:%M - %d/%m/%y')
  end

  def accounts_count
    current_user.accounts.count
  end

  private

  def user_initial(user)
    "#{user.full_name.first.downcase}.png"
  end
end
