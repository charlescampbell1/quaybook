# frozen_string_literal: true

class CreateAccounts < ActiveRecord::Migration[6.0]
  def change
    create_table :accounts do |t|
      t.belongs_to :user

      t.string :name, null: false
      t.string :street_address, null: false
      t.string :city, null: false
      t.string :county, null: false
      t.string :postcode, null: false
      t.string :contact_number, null: false
      t.string :email, null: false
      t.string :notes
      t.boolean :active, null: false, default: true

      t.timestamps
    end
  end
end
