# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.8.2](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.8.1...v1.8.2) (2021-05-12)

### [1.8.1](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.8.0...v1.8.1) (2021-05-03)

## [1.8.0](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.7.2...v1.8.0) (2021-05-03)


### Features

* **metrics:** Add prometheus /metrics endpoint ([b75bb0c](https://gitlab.com/charlescampbell1/quaybook/-/commit/b75bb0c949fa239399ae232300eb07732565fdf5))

### [1.7.2](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.7.1...v1.7.2) (2021-04-28)

### [1.7.1](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.7.0...v1.7.1) (2021-04-28)


### Bug Fixes

* **user:** Fixed issue where users could not edit profile ([e57c3f7](https://gitlab.com/charlescampbell1/quaybook/-/commit/e57c3f7f46c6ec511fa80e7653cfb37de7dc0501))

## [1.7.0](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.6.3...v1.7.0) (2021-04-28)


### Features

* **bookkeeping:** Add annual summary ([def9a7e](https://gitlab.com/charlescampbell1/quaybook/-/commit/def9a7eb983b8b5df126bc240c79171028c2c190))


### Bug Fixes

* **paye_income:** Resolve issue where April inputs could result in error ([db825a8](https://gitlab.com/charlescampbell1/quaybook/-/commit/db825a893063cc9eba094af0c44f044d39986b4d))

### [1.6.3](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.6.2...v1.6.3) (2021-04-22)


### Bug Fixes

* **people:** Pagination broken on user page, temporary remove ([5c48d1a](https://gitlab.com/charlescampbell1/quaybook/-/commit/5c48d1aa16b863d1167f7e6da12415bc614ea506))

### [1.6.2](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.6.1...v1.6.2) (2021-04-22)


### Bug Fixes

* **invoice:** Fix issue where invoice page crashes ([d03652b](https://gitlab.com/charlescampbell1/quaybook/-/commit/d03652bce3be24e85630e12ad8195abd6710ba66))

### [1.6.1](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.6.0...v1.6.1) (2021-04-22)


### Bug Fixes

* **invoice:** Hide approval button if invoice is still being worked on ([eb0997a](https://gitlab.com/charlescampbell1/quaybook/-/commit/eb0997a39cde1c48753caeca1ef06e5b18319c59))
* **pagination:** Fix issue where pages with old style pagination stopped working ([ba6a693](https://gitlab.com/charlescampbell1/quaybook/-/commit/ba6a693ea148f052dce3d861c50da56ea2e8c0ba))

## [1.6.0](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.5.2...v1.6.0) (2021-03-18)


### Features

* **invoice:** Give users the ability to edit the details of an invoice ([47a0efb](https://gitlab.com/charlescampbell1/quaybook/-/commit/47a0efbb791919c05184968df9a0f3f2d7d9905b))

### [1.5.2](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.5.1...v1.5.2) (2021-03-18)


### Bug Fixes

* **item:** Fixed issue where item could not be added ([ff879d3](https://gitlab.com/charlescampbell1/quaybook/-/commit/ff879d3940b2f70c9f88fbc9a02765407f59404f))

### [1.5.1](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.5.0...v1.5.1) (2021-03-18)


### Bug Fixes

* **item:** Stop setting default balance to 0.0 ([41e2fdb](https://gitlab.com/charlescampbell1/quaybook/-/commit/41e2fdb4e95b7c8aceee52e610b9ac464998200d))

## [1.5.0](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.4.0...v1.5.0) (2021-03-16)


### Features

* **features:** Control quaybook features through flags ([b16df0a](https://gitlab.com/charlescampbell1/quaybook/-/commit/b16df0aeb30dbd55c72dc1b7974829ea048dfaf0))
* **invoice:** Notify account users when an invoice changes state ([a3e9008](https://gitlab.com/charlescampbell1/quaybook/-/commit/a3e90083e10e012a381cd009e10183cb48480359))


### Bug Fixes

* **holding:** Corrected the too many redirects issues ([2eccd66](https://gitlab.com/charlescampbell1/quaybook/-/commit/2eccd66b162ef0d689d92ef5a1b5635b862ef53f))

## [1.4.0](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.3.0...v1.4.0) (2021-03-11)


### Features

* **paye income:** Create, update and delete ([dc19088](https://gitlab.com/charlescampbell1/quaybook/-/commit/dc190882dc61447da770c029718624c5dec7c727))

## [1.3.0](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.2.0...v1.3.0) (2021-03-11)


### Features

* **dashboard:** Add current year to date finances widget ([eb088fb](https://gitlab.com/charlescampbell1/quaybook/-/commit/eb088fb97a4a02b9f7a5473a4b955c2ba3922a96))
* **dashboard:** Add invoice widget ([7b1d6f0](https://gitlab.com/charlescampbell1/quaybook/-/commit/7b1d6f02c2988a4bbdc966723a86a20e7c8cb76d))
* **dashboard:** Add tax helper widget ([abe9858](https://gitlab.com/charlescampbell1/quaybook/-/commit/abe98589964b9c633fc1c7e121c01d4e83a1697d))

## [1.2.0](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.1.1...v1.2.0) (2021-03-07)


### Features

* **invoice:** Adding an invoice that is paid appears in bookkeeping ([87993d0](https://gitlab.com/charlescampbell1/quaybook/-/commit/87993d052c715d226170e2e6b0838ce7dd2d36b2))
* **invoice:** Users can now raise an invoice without items ([6ac5ffd](https://gitlab.com/charlescampbell1/quaybook/-/commit/6ac5ffd27d91db9f52a4eff644ba205d1fa685ef))

### [1.1.1](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.1.0...v1.1.1) (2021-03-06)

## [1.1.0](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.0.3...v1.1.0) (2021-03-06)


### Features

* **user:** Allow users to edit their own profile and display pictures ([3ee9edc](https://gitlab.com/charlescampbell1/quaybook/-/commit/3ee9edc3bb28d1a81694a0ea112b02c961260876))

### [1.0.3](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.0.2...v1.0.3) (2021-03-06)

### [1.0.2](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.0.1...v1.0.2) (2021-03-06)


### Bug Fixes

* **assets:** Specify file type for user icons ([4c1e10b](https://gitlab.com/charlescampbell1/quaybook/-/commit/4c1e10bf541ff4b7fa28bd3d9fb8ca40b621f1e6))

### [1.0.1](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.0.0...v1.0.1) (2021-03-06)


### Bug Fixes

* **project:** Specify ruby version ([46ea9a2](https://gitlab.com/charlescampbell1/quaybook/-/commit/46ea9a263e7239c94fa6784a3a9c6d637d66088f))

### [1.0.0](https://gitlab.com/charlescampbell1/quaybook/-/compare/v1.0.0...v0.3.1) (2021-03-06)

## [0.3.0](https://gitlab.com/charlescampbell1/quaybook/-/compare/v0.2.3...v0.3.0) (2021-03-05)


### ⚠ BREAKING CHANGES

* **project:** Bump ruby to version 3

* **project:** Bump ruby to version 3 ([cfbe2b6](https://gitlab.com/charlescampbell1/quaybook/-/commit/cfbe2b68f33e4d2ba2f48c857adb224223693a03))

### [0.2.3](https://gitlab.com/charlescampbell1/quaybook/-/compare/v0.2.2...v0.2.3) (2021-03-05)


### Bug Fixes

* **user:** AJAX searching is now working ([6ecb0e5](https://gitlab.com/charlescampbell1/quaybook/-/commit/6ecb0e550277a2f74e36f0e21f8d1f0935d4459e))
* **users:** Hide already registered users from add to account selection ([eb857ee](https://gitlab.com/charlescampbell1/quaybook/-/commit/eb857ee1467c2d316bf7af53654a98b3195cc435))

### [0.2.2](https://gitlab.com/charlescampbell1/quaybook/-/compare/v0.2.1...v0.2.2) (2021-03-05)


### Bug Fixes

* **routes:** Reproritize users resources ([6833eeb](https://gitlab.com/charlescampbell1/quaybook/-/commit/6833eeb74c40e3048f3434e98dde4758a7775c20))

### [0.2.1](https://gitlab.com/charlescampbell1/quaybook/-/compare/v0.2.0...v0.2.1) (2021-02-27)

## 0.2.0 (2021-02-21)


### Features

* **authentication:** Add & configure devise gem ([94f7166](https://gitlab.com/charlescampbell1/quaybook/-/commit/94f7166d854a24e5e99ddad84d412100017a1569))
* **authentication:** Add Material UI for Sign In page ([59db096](https://gitlab.com/charlescampbell1/quaybook/-/commit/59db09617792a823d3a0387da8e4f35772c61bdb))
* **authentication:** Add Material UI for Sign Up page ([38ad0bf](https://gitlab.com/charlescampbell1/quaybook/-/commit/38ad0bf049092b5f6e7eebe47ad4ca389764a79c))

### 0.1.0 (2021-02-10)
