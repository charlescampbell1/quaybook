class CreateInvoices < ActiveRecord::Migration[6.0]
  def change
    create_table :invoices do |t|
      t.string :title,                    null: false
      t.string :category
      t.string :customer_name,            null: false
      t.string :customer_contact_number,  null: false
      t.string :notes
      t.date :issued_on,                  null: false
      t.date :due_by,                     null: false
      t.date :paid_on
      t.decimal :balance,                 null: false, default: 0
      t.decimal :vat,                     null: false, default: 0
      t.string :raised_by,                null: false
      t.boolean :paid,                    null: false, default: false
      t.boolean :approved,                null: false, default: false
      t.boolean :pending_approval,        null: false, default: false

      t.timestamps
    end
  end
end
