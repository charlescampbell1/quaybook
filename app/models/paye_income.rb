# frozen_string_literal: true

class PayeIncome < ApplicationRecord
  has_and_belongs_to_many :accounts
  has_and_belongs_to_many :months
end
