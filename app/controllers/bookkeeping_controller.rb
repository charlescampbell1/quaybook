# frozen_string_literal: true

class BookkeepingController < ApplicationController
  include BookkeepingHelper
  include AccountHelper

  before_action :authenticate_user!, :active_user?, :active_account?
  before_action :set_year, only: %i[show_year show_summary]
  before_action :set_month, only: %i[show_month]

  def index
    @years = selected_account.years.order('year DESC')

    render 'components/pages/bookkeeping/index'
  end

  def new_year
    @year = Year.new

    render 'components/pages/bookkeeping/new'
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create_year
    year = Year.new(year_params)

    return redirect_to bookkeeping_path, alert: "#{year.year} already exists." if year_exists?(year)

    respond_to do |format|
      if year.save
        map_year_to_account(year)
        generate_months_for(year)

        format.html { redirect_to bookkeeping_path, notice: "#{year.year} created." }
      else
        format.html { redirect_to bookkeeping_path, alert: "Error: #{year.errors.to_a}" }
      end
    end
  end

  def show_year
    render 'components/pages/bookkeeping/year'
  end

  def show_month
    income = @month.incomes
    expenses = @month.expenses
    paye_incomes = @month.paye_incomes

    @records = income + expenses + paye_incomes
    @records.sort_by(&:date_of)

    render 'components/pages/bookkeeping/month'
  end

  private

  def year_params
    params.require(:year).permit(:year)
  end

  def set_year
    @year = Year.find(params[:id])
  end

  def set_month
    @month = Month.find(params[:id])
  end
end
