class CreateNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :notifications do |t|
      t.string :notification, null: false
      t.string :category,     null: false
      t.string :sender,       null: false
      t.boolean :read,        null: false, default: false

      t.timestamps
    end
  end
end
