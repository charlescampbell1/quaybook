# frozen_string_literal: true

module Finances
  class FinancialYear
    def initialize(account:, date:)
      @date = date
      @account = account
    end

    def find_or_create
      years = account.years.where(year: formatted_year)

      if years.empty?
        create
      else
        years.first
      end
    end

    def create
      year = Year.create(year: formatted_year)

      add_months_to(year)

      account.years << year

      year
    end

    def add_months_to(year)
      i = 0

      while i < tax_year_months.count
        month = Month.create(month: formatted_month(i, start_year(year)))

        year.months << month

        i += 1
      end
    end

    attr_reader :account, :date

    private

    # Returns the financial year in the tax format i.e. 2020/21
    def formatted_year
      month_number = date.strftime('%m').to_i
      year = date.strftime('%y').to_i

      return "20#{year}/#{year + 1}" if month_number > 4

      "20#{year - 1}/#{year}"
    end

    # Returns the month in tax format i.e. April '20
    def formatted_month(counter, year)
      "#{tax_year_months[counter]} '#{set_year(year, counter)}"
    end

    def set_year(year, counter)
      return year if counter < 9

      (year.to_i + 1).to_s
    end

    def tax_year_months
      %w[April May June July August September October November December January February March April]
    end

    def start_year(financial_year)
      financial_year.year[2..3]
    end
  end
end
