# frozen_string_literal: true

module SupportHelper
  def support_options
    %w[fix feature]
  end

  def raise_trello_ticket(label:, title:, description:)
    label = 'feat' if label.eql?('feature')

    trello_client.add_card_by_name!(
      board_name: 'Quaybook Support Requests',
      list_name: 'Backlog',
      label_name: label,
      title: "#{label}: #{title}",
      description: description_formatter(description)
    )
  end

  private

  def trello_client
    Trello::Client.new(key: ENV.fetch('TRELLO_KEY'), token: ENV.fetch('TRELLO_TOKEN'))
  end

  def description_formatter(description)
    <<~DOC
      #{description}

      User details
      Name: #{current_user.full_name}
      Email: #{current_user.email}
    DOC
  end
end
