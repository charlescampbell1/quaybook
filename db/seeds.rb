# frozen_string_literal: true

charlie = User.create(
  full_name: 'Charlie Campbell',
  email: 'charlie.campbell14@gmail.com',
  password: 'password',
  contact_number: '07123456789',
  street_address: '2 The Mews, 10 Norwich Avenue',
  city: 'Bournemouth',
  county: 'Dorset',
  postcode: 'BH2 5QG',
  active: true,
  status: 'admin'
)

account = Account.create(
  name: 'QuayBooks',
  street_address: '123 Business Lane',
  city: 'Truro',
  county: 'Cornwall',
  postcode: 'TR4 1XX',
  contact_number: '123123123',
  email: 'info@quaybook.com',
  active: true
)

account.users << charlie