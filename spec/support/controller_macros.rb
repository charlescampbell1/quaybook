# frozen_string_literal: true

module ControllerMacros
  def login_user
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]

      user = FactoryBot.create(:user)
      account = FactoryBot.create(:account)

      user.accounts << account
      user.save

      sign_in user
    end
  end

  def login_inactive_user
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]

      user = FactoryBot.create(:user)
      user.active = false
      account = FactoryBot.create(:account)

      user.accounts << account
      user.save

      sign_in user
    end
  end

  def logout_user
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      user = FactoryBot.create(:user)
      sign_out user
    end
  end

  def login_admin
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]

      user = FactoryBot.create(:user)
      account = FactoryBot.create(:account)

      user.accounts << account
      user.status = 'admin'
      user.save

      sign_in user
    end
  end
end
