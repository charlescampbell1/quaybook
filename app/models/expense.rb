# frozen_string_literal: true

class Expense < ApplicationRecord
  has_and_belongs_to_many :months
end
