# frozen_string_literal: true

module FeatureHelper
  def feature_enabled?(feature)
    return false if selected_account.nil?

    result = selected_account.features.where(code_id: feature)

    return false if result.empty?

    true
  end

  def feature_status_for(account, feature)
    @account = account
    @feature = feature

    return enabled if account_has_feature?

    disabled
  end

  def feature_enabled_for(account, feature)
    @account = account
    @feature = feature

    return true if account_has_feature?

    false
  end

  def which_feature_toggle(account, feature)
    @account = account
    @feature = feature

    return disable if account_has_feature?

    enable
  end

  private

  def account_has_feature?
    result = @account.features.where(id: @feature.id)

    return false if result.empty?

    true
  end

  def enable
    link_to 'Enable', enable_feature_path(@feature, @account)
  end

  def disable
    link_to 'Disable', disable_feature_path(@feature, @account)
  end

  def disabled
    '<span class="badge badge-danger">OFF</span>'.html_safe
  end

  def enabled
    '<span class="badge badge-success">ON</span>'.html_safe
  end
end
