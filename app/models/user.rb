# frozen_string_literal: true

class User < ApplicationRecord
  has_and_belongs_to_many :accounts
  has_and_belongs_to_many :notifications

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable, :trackable

  has_one_attached :avatar

  validates_presence_of :full_name, :email

  def self.search(search)
    if search
      where('LOWER(full_name) LIKE ?', "%#{search.downcase}%")
    else
      where(nil)
    end
  end
end
