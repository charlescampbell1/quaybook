# frozen_string_literal: true

module InvoicesHelper
  def invoice_raised_by(invoice)
    user = User.find(invoice.raised_by)

    user.full_name
  end

  def invoice_status(invoice)
    @invoice = invoice

    return '<span class="badge badge-success">INVOICE PROCESSED</span>'.html_safe if settled
    return '<span class="badge badge-warning">AWAITING PAYMENT</span>'.html_safe if with_customer

    '<span class="badge badge-danger">AWAITING APPROVAL</span>'.html_safe
  end

  def invoice_due_date(invoice)
    return '<span class="badge badge-success">PAID</span>'.html_safe if invoice.paid
    return '<span class="badge badge-danger">PAST DUE</span>'.html_safe if invoice.due_by < Time.now

    invoice.due_by.strftime('%d/%m/%Y')
  end

  def account_invoices
    selected_account.invoices
  end

  def request_approval_button(invoice)
    return nil if invoice.approved
    return nil if invoice.paid
    return nil if invoice.items.empty?
    return nil if invoice.pending_approval

    link_to 'Request Approval', request_approval_path(invoice), class: 'submit-button button-success'
  end

  def approval_button(invoice)
    @invoice = invoice

    return nil unless invoice.pending_approval.eql?(true)
    return nil if current_user.status.eql?('user')
    return nil if invoice.items.empty?
    return link_to 'Approve', approve_invoice_path(invoice), class: 'submit-button button-success' if requires_approval

    nil
  end

  def decline_button(invoice)
    return nil if current_user.status.eql?('user')
    return nil if requires_approval
    return nil if invoice.paid

    link_to 'Decline', reject_invoice_path(invoice), class: 'submit-button button-danger'
  end

  def pay_button(invoice)
    return nil if invoice.paid
    return nil if requires_approval

    modal_link_to 'MARK AS PAID', pay_invoice_confirm_path(invoice), style: 'submit-button button-success'
  end

  def edit_button(invoice)
    return nil if invoice.paid
    return nil if invoice.approved

    link_to 'Edit', edit_invoice_path(invoice), class: 'submit-button'
  end

  def delete_button(invoice)
    return nil if invoice.paid
    return nil if invoice.approved

    link_to 'Delete', destroy_invoice_path(invoice),
            class: 'submit-button button-danger',
            data: { confirm: 'Click OK to delete this invoice. This action cannot be undone.' }
  end

  def dashboard_invoices
    selected_account.invoices.where(paid: false)
  end

  private

  def requires_approval
    return true unless @invoice.approved
  end

  def settled
    return true if @invoice.paid && @invoice.approved
  end

  def with_customer
    return true if @invoice.approved
  end
end
