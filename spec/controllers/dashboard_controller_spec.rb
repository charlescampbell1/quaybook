# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DashboardController do
  describe '#index' do
    login_user

    before { get :index }

    it 'renders the index template' do
      expect(response).to render_template('index')
    end
  end

  describe '#holding' do
    login_inactive_user

    it 'renders the holding template' do
      get :holding

      expect(response).to render_template('inactive_holding')
    end
  end
end
