# frozen_string_literal: true

require 'rails_helper'

RSpec.describe BookkeepingController do
  login_user

  describe '#index' do
    it 'renders the index template' do
      get :index

      expect(response).to render_template('index')
    end
  end

  describe '#new_year' do
    it 'assigns a new year to @year' do
      get :new_year, format: :js, xhr: true

      expect(assigns(:year)).to be_a_new(Year)
    end
  end

  describe '#create_year' do
    context 'with valid attributes' do
      it 'creates a new year' do
        expect { post :create_year, params: { year: FactoryBot.attributes_for(:year) } }
          .to change(Year, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      it 'does not create a new year' do
        expect { post :create_year, params: { year: FactoryBot.attributes_for(:year, year: nil) } }
          .to change(Year, :count).by(0)
      end
    end
  end

  describe '#show_year' do
    it 'displays the year information' do
      get :show_year, params: { id: 1 }

      expect(response).to render_template('year')
    end
  end

  describe '#show_month' do
    it 'displays the month information' do
      get :show_month, params: { id: 1 }

      expect(response).to render_template('month')
    end
  end
end
